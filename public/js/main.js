; (function () {

	let canvas = document.getElementById('button');
	let ctx = canvas.getContext('2d');
	ctx.canvas.width = 211;
	ctx.canvas.height = 36;

	bW = ctx.canvas.width;
	bH = ctx.canvas.height;
	ctx.fillStyle = "white";
	ctx.fillRect(0, 0, bW, bH);



	const randomNumber = function (num) {
		let number = Math.random() * num + 1;
		return number;
	};

	const mouseCoordinates = function (evt) {
		var rect = canvas.getBoundingClientRect();
		return {
			x: Math.floor((evt.clientX - rect.left) / (rect.right - rect.left) * canvas.width),
			y: Math.floor((evt.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height)
		};
	};

	const fillBackground = function () {
		let buttonGradient = ctx.createLinearGradient(0, 0, 0, bH);
		buttonGradient.addColorStop(0, "#52cdf6");
		buttonGradient.addColorStop(0.91, "#05a0e2");
		ctx.beginPath();

		ctx.fillStyle = buttonGradient;
		ctx.fillRect(0, 0, bW, bH);
	};

	const writeText = function () {
		ctx.beginPath();
		ctx.fillStyle = "#FFFFFF";
		ctx.font = "16px Arimo";
		ctx.shadowBlur = 2;
		ctx.shadowColor = "black";
		ctx.fillText("Download Solution Brief", 19, 23);
	};


	const randomDirection = function () {
		let number = randomNumber(4) - 2;
		return number;
	};

	let mouseOnButton = false;

	const particleContainer = [];

	const Particle = function (x, y, radius, speed, color) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.speed = speed;
		this.color = color;
		this.moveX = randomDirection();
		this.moveY = randomDirection();


		this.pulse = function () {
			ctx.beginPath();
			ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
			ctx.fillStyle = color;
			ctx.fill();
			this.x += this.moveX;
			this.y += this.moveY;
			if (this.x >= bW) {
				this.moveX = -this.moveX;
			}
			if (this.x <= 0) {
				this.moveX = -this.moveX;
			}
			if (this.y >= bH) {
				this.moveY = -this.moveY;
			}
			if (this.y <= 0) {
				this.moveY = -this.moveY;
			}
		};

		this.attractParticles = function (event) {		
			let mousePos = mouseCoordinates(event);

			if (this.x > mousePos.x) {
				this.x -= 3;
			} else {
				this.x += 3;
			}
			if (this.y > mousePos.y) {
				this.y -= 3;
			} else {
				this.y += 3;
			}


		};

	};

	const drawParticles = function () {
		for (let i = 0; i < particleContainer.length; i++) {
			particleContainer[i].pulse();
		}
	};
	let event;
	const checkMousePosition = function () {
		if (mouseOnButton) {
			for (let i = 0; i < particleContainer.length; i++) {
				particleContainer[i].attractParticles(event);
			}
		} else {
			return
		}
	};

	const animateButton = function () {
		fillBackground();
		drawParticles();
		writeText();
		checkMousePosition();
		window.requestAnimationFrame(animateButton);
	};

	const addListeners = function () {
		canvas.addEventListener('mousemove', function (e) {
			mouseOnButton = true;
			event = e;
		});
		canvas.addEventListener('mouseleave', function (e) {
			mouseOnButton = false;
		});
		canvas.addEventListener('click', function (e) {
			mouseOnButton = false;
			window.open("https://iq.quantum.com/exLink.asp?46849376OZ32L25I160985874&SB00171A&view=1", "_blank");
			
		});
	};

	const colorArray = ['#ED302E', '#ED302E', '#1D1D1D', '#46C6F3', '#ED302E', '#1D1D1D'];

	const randomColor = function () {
		return colorArray[Math.floor(Math.random() * 6)];
	}; 

	const createParticles = function () {
		for (let i = 0; i < randomNumber(50) + 40; i++) {
			let xPos = randomNumber(bW);
			let yPos = randomNumber(bH);
			let radius = 2;
			let speed = randomNumber(3) + 1;
			particleContainer.push(new Particle(xPos, yPos, radius, speed, randomColor()));
		}
	};

	const init = function () {
		addListeners();
		createParticles();
		window.requestAnimationFrame(animateButton);


	};

	window.onload = init;

})();