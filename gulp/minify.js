const gulp = require('gulp');
const cleanCss = require('gulp-clean-css');
const path = require('./config');
const uglify = require('gulp-uglify');
const clean = require('del');
const minifyHtml = require('gulp-minify-html');
const concat = require('gulp-concat');
const babel = require('lb-gulp-es2015');


gulp.task('clean', function (done) {
	return clean([path.gulp.paths.dist], done);
});

gulp.task('scripts', function () {
	if (process.env.NODE_ENV != "production") {
		console.log('in scripts debug');
		return gulp.src([
			path.gulp.paths.src + '/js/*.js'
		])
			.pipe(babel())
			.pipe(concat('all.min.js'))
			.pipe(gulp.dest(path.gulp.paths.dist));
	} else {
		console.log('in scripts production');
		return gulp.src([
			path.gulp.paths.src + '/js/*.js',
		])
			.pipe(babel())
			.pipe(concat('all.min.js'))
			.pipe(uglify())
			.pipe(gulp.dest(path.gulp.paths.dist));
	}
});

gulp.task('css', function () {
	return gulp.src(path.gulp.paths.src + '/stylesheets/*.css')
		.pipe(cleanCss())
		.pipe(gulp.dest(path.gulp.paths.dist));
});

gulp.task('html', function () {
	return gulp.src(path.gulp.paths.src + '/html/*.html')
		.pipe(minifyHtml())
		.pipe(gulp.dest(path.gulp.paths.dist));
});

gulp.task('images', function () {
	return gulp.src(path.gulp.paths.src + '/images/*.jpg')
		.pipe(gulp.dest(path.gulp.paths.dist));
});

gulp.task('buildApp', ['html', 'css', 'scripts', 'images']);

gulp.task('build', ['watch'], function () {
	gulp.start('buildApp');
});

gulp.task('productionBuild', ['watch'], function () {
	process.env.NODE_ENV = "production";
	gulp.start('buildApp');
});

gulp.task('watch', function () {
	gulp.watch(path.gulp.paths.src + '/**/*.css', ['css']);
	gulp.watch(path.gulp.paths.src + '/**/*.html', ['html']);
	gulp.watch(path.gulp.paths.src + '/**/*.js', ['scripts']);
	gulp.watch(path.gulp.paths.src + '/**/*.jpg', ['images']);
});

console.log('Finished gulp tasks');