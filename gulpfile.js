﻿'use strict'
var gulp = require('gulp');
require('require-dir')('./gulp');

gulp.task('default', ['clean'], function () {
    process.env.NODE_ENV = null;
    gulp.start('build');
});

gulp.task('prod', ['clean'], function () {
    process.env.NODE_ENV = "production";
    gulp.start('build');
});